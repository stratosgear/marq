from fastapi import FastAPI
from .schemas import Result, Queue, Job
from typing import List, Optional, Dict, Union, Any
from marq.connections import ArqRedis, create_pool, redis_port, redis_host, redis_db
from jsonpickle import decode, encode
import re
import os
import json

q_name_regex = r"^arq:queue:([a-zA-Z_]+):?(.*)$"
q_name_matcher = re.compile(q_name_regex)

q_contents_regex = r"(.*)\sj_complete=(\d+)\sj_failed=(\d+)\sj_retried=(\d+)\sj_ongoing=(\d+)\squeued=(\d+)"
q_contents_matcher = re.compile(q_contents_regex)

redis = ArqRedis(
        host=redis_host,
        port=redis_port,
        db=redis_db,
        encoding='utf-8',
        decode_responses=True,
        # job_serializer=encode,
        # job_deserializer=decode
    )

DEFAULT_CACHE_DURATION = 4
try:
    CACHE_DURATION = int(os.getenv("MARQ_API_CACHE_SECS", DEFAULT_CACHE_DURATION))
except:
    CACHE_DURATION = DEFAULT_CACHE_DURATION

marq_api = FastAPI()

@marq_api.get("/state", response_model=List[Queue])
async def get_state(archived: bool = False) -> List[Dict]:

    cached_reply = await redis.get("arq:cached_reply")
    if cached_reply:
        return json.loads(cached_reply)
    else:
        queues = await _get_queues()

        items : List[Result] = await _get_results() if archived else await _get_jobs()

        if items:
            for i in items:
                q = queues.get(i.queue)
                if q:
                    q.jobs.append(i)
        # We sort the queues AND return them as dicts, so that the simplistic serializer in
        # FastapiRedisCache will NOT complain about not being to serialize our Pydantic
        # models in our response.  Fastapi, nonetheless, will convert the dicts back to the
        # proper response_models afterwards!
        queues_sorted = [queues[q].dict() for q in sorted(queues)]

        await redis.setex("arq:cached_reply", CACHE_DURATION, json.dumps(queues_sorted))
        return queues_sorted


async def _get_queues() -> Dict[str, Queue]:

    reply = {}
    queue_names = []

    cur = b"0"
    while cur:
        cur, keys = await redis.scan(cur, "arq:queue:*")
        for key in keys:
            if m := q_name_matcher.match(key):
                queue_name = m.group(1)

                q = Queue(name=queue_name)

                # Try to read queue info from the ":health-check" key, if any
                q_health = await redis.get(f"arq:queue:{queue_name}:health-check")

                if q_health:
                    if contents := q_contents_matcher.match(q_health):

                        last_ping = contents.group(1)

                        q.completed = contents.group(2)
                        q.failed = contents.group(3)
                        q.retried = contents.group(4)
                        q.ongoing = contents.group(5)
                        q.queued = contents.group(6)

                if queue_name not in queue_names:
                    queue_names.append(queue_name)
                    reply[queue_name] = q

    return reply


async def _get_jobs() -> List[Job]:

    reply = []
    cur = b"0"
    while cur:
        cur, keys = await redis.scan(cur, "arq:job:*")
        for k in keys:

            job_id_num = _extract_id_from_str(k)
            job_data_str = await redis.get(k)
            job_running = await redis.get(f"arq:in-progress:{job_id_num}")

            job_data = json.loads(job_data_str)
            q = job_data.get("q")

            jid = _extract_id_from_str(k)
            job = _to_job(id=jid, item=job_data, start_time=job_running)
            job_log_entries = await redis.lrange(f"arq:job_log:{job_id_num}", 0, -1)
            job.log = job_log_entries
            reply.append(job)

    reply = sorted(reply, key=lambda j: j.score)
    return reply


def _to_job(id: str, item: Dict, start_time: int) -> Job:
    job = Job(id=id, queue=_extract_queuename_from_str(item.get("q")))
    job.running = bool(start_time)

    job.name = item.get("n")

    job.function = _extract_function_name(item.get("f"))
    job.args = item.get("a")
    job.kwargs = item.get("k")

    job.enqueue_time = item.get("et")

    job.start_time = start_time
    job.parent = item.get("pj")
    job.score = item.get("sc")
    job.priority = int(item.get("p"))
    job.tries = item.get("t")

    return job



async def _get_results() -> List[Result]:

    reply = []
    cur = b"0"
    while cur:
        cur, keys = await redis.scan(cur, "arq:result:*")
        for k in keys:

            job_data_str = await redis.get(k)

            job_data = json.loads(job_data_str)

            jid = _extract_id_from_str(k)
            result = _to_result(id=jid, item=job_data)

            reply.append(result)

    reply = sorted(reply, key=lambda j: j.finish_time, reverse=True)

    return reply


def _to_result(id: str, item: Dict) -> Result:

    result = Result(id=id, queue=_extract_queuename_from_str(item.get("q")))
    result.name = item.get("n")
    result.function = _extract_function_name(item.get("f"))
    result.args = item.get("a")
    result.kwargs = item.get("k")

    result.enqueue_time = item.get("et")

    result.parent = item.get("pj")
    result.score = item.get("sc")
    result.priority = int(item.get("p"))
    result.tries = item.get("t")

    result.result = item.get("r")
    result.success = bool(item.get("s"))

    result.start_time = item.get("st")
    result.finish_time = item.get("ft")
    result.log = item.get("l", [])
    result.children_jobs = item.get("sj", [])
    return result

def _extract_id_from_str(s: str) -> str:
    """Take something that looks like arq:result:XXXXX and return XXXXX"""

    try:
        return s[s.rindex(":") + 1 :]
    except ValueError:
        # Fails when we cannot match the ":" search
        return s


def _extract_queuename_from_str(s: str) -> str:
    """Takes something that looks like arq:queue:XXXXX and return XXXXX"""

    try:
        return s[s.rindex(":") + 1 :]
    except ValueError:
        # Fails when we cannot match the ":" search
        return s

def _extract_function_name(f: Any) -> str:

    if isinstance(f, str):
        return f
    else:
        print("================================================")
        print(f)
        print(type(f))
        print(encode(f))
        return json.loads(encode(f)).get("name")