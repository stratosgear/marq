from pydantic import BaseModel
from typing import List, Union, Dict, Any, Optional

class Job(BaseModel):
    id: str
    function: Optional[str] = None
    name: Optional[str] = None
    queue: str
    tries: Optional[int]
    args: Dict[Any, Any] = {}
    kwargs: Union[Dict, Any] = {}
    enqueue_time: Optional[int]
    start_time: Optional[int]
    finish_time: Optional[int]
    score: Optional[int]
    priority: int = 5
    parent: Optional[str] = None
    running: bool = False
    log: Optional[List[str]] = []
    raw: Dict = {}


class Result(Job):
    success: bool = True
    result: Any
    finish_time: Optional[int]
    log: Optional[List[str]] = None
    children_jobs: Optional[List[str]] = None


class Queue(BaseModel):
    name: str
    completed: int = 0
    failed: int = 0
    retried: int = 0
    ongoing: int = 0
    queued: int = 0
    jobs: Optional[List[Result]] = []
