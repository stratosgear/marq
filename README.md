# marq

Job queues in python with asyncio and redis

Based on the [samuelcolvin](https://github.com/samuelcolvin) version of [arq](https://github.com/samuelcolvin/arq)


See [original documentation](https://arq-docs.helpmanual.io/) for the majority of the details.

## Differences

* Project name:
  * *marq: Modified Arq*
* Ability to provide a friendly 'job_name' to Jobs.
* The Jobs now contain the queue name too, not only the JobResults.
* Defaults to [jsonpickle](https://jsonpickle.github.io/index.html) for serialization and
  deserialization
    * This way, different processes can read the contents of the Redis Arq serialized
  items **without** the need to locally have the python code used in the queues.
* Add Job logging ability:
  * The `ctx` variable now contains a `logger` method that can be used as:
    * job_logger = ctx.get("logger")
    * await job_logger("Some log message") or
    * await job_logger("This looks fishy!", level="warning") or
  * The job log is kept as a Redis list under the `arg:job:log:[job_id]` key (key
      configured by editing `constants.py:job_log_key_prefix`)
  * The job log is kept as a separate redis key while the job is ongoing, then removed.
  * The job log array is persisted in the Job Results when the job is done.
* Jobs can optionally be linked to each other with `parent_job_id`s.
  * Usage: `await redis.enqueue_job(..., _parent_job_id="XXX")`
  * JobResults now contain two extra attributes:
    * `parent_job_id`: The parent job id (if any)
    * `subjobs_ids`: A list of subjobs ids (if any)
* Job score is also saved in the Job object
* Jobs can be prioritized. Priority levels are values in the range (1-5), inclusive. 1
  is the highest priority level, while 5 the lowest. Defaults to 5 (keeps compatibility
  with arq).
  * Used by passing a `_priority` param to `enqueue_job`
  * This is implemented by manipulating the original `defer_by` param:
    * 5 (lowest) priority does not change the `defer_by` at all
    * Each additional priority level sets the `defer_by` by another multiple of 365 days
      (one year)
    * Thus, Priority 1 (highest) sets `defer_by` by -4*365 days (four years)
  * The context param `ctx` will also contain a priority attribute.  This way a task
    that might need to potentially schedule more tasks, can utilize the same priority
    level as itself, if it needs to.

## Extra Features

### Fastapi sub-application

The Marq package now includes a Fastapi sub-application, that returns an internal
respresentation of the Arq state (or ir it Marq state now?).  It can be mounted at any
custom path of an existing Fastapi application, and it provides a single API endpoint
`/state`

There is a separate application, [marqui](https://gitlab.com/stratosgear/marqui) that is
a Graphical Interface of the Marq library that is making exclusive use of the API
provided here.  Of course the API responses can be used for any other custom needs.

#### Usage

From an existing Fastapi application, import the Marq sub-app and mount it at an
endpoint of your liking, likewise:

```
from fastapi import FastAPI
from marq.fastapi import subapp

app = FastaAPI()

app.mount(path="/any/mountpoint/marq", app=subapp, name="marq_api")
```

from that point on, the endpoint `/any/mountpoint/marq/state` will be returning a list
Of Queue objects, each on containing a list of Jobs.  As with all Fastapi applications,
the url at `/any/mountpoint/marq/docs` provides detailed openapi documentation site that
fully describes the API call.

#### Advanced usage

If you have heavy usage of the Marq API calls, then it is better if you can cache the
API responses, since getting a full represenation of the whole internal Marq state could
be quite taxing on Redis, depending on the amount of queues and jobs you might have.

For this purpose the API call comes already preconfigured with the
[fastapi-redis-cache](https://github.com/a-luna/fastapi-redis-cache) that allows the api
called to be cached for a user defined amount of time.

In order to utlize the cache though, you will have to manually instantiate and init
`fastapi-redis-cache` in your
[on_event("startup")](https://fastapi.tiangolo.com/advanced/events/) event handler of
your main Fastapi App, according to the `fastapi-redis-cache` instructions.  It could be
as easy as:

```
LOCAL_REDIS_URL = "redis://127.0.0.1:6379"
@app.on_event("startup")
def startup():
    redis_cache = FastApiRedisCache()
    redis_cache.init(
        host_url=os.environ.get("REDIS_URL", LOCAL_REDIS_URL),
        prefix="myapi-cache",
        response_header="X-MyAPI-Cache",
        ignore_arg_types=[Request, Response, Session]
    )
```

#### Configuring the cache time

Once this is done then the `marq_api` calls will be able to be cached for a configurable
amount of time.  You can configure the amount of caching by providing an environment
variable called:

* `MARQ_API_CACHE_SECS`

that defaults to 4 secs if the environment variable is not set (or **no** caching at all
if you have not inited the `fastapi-redis-cache` caching package.)

The api results provided at the API endpoint `some/mountpoint/state`